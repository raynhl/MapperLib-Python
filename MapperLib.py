from math import *


class Mapper:
	def __init__(self, x = -1292778.69951722, y = 6235923.25115904, z = 348991.679260659):
		self.x = x;
		self.y = y;
		self.z = z;

	def printReferencePoints(self):
		print("{0:.20f}".format(self.x))
		print("{0:.20f}".format(self.y))
		print("{0:.20f}".format(self.z))

	def convertToGps(self, x, y, z):
		a = 6378137.0
		f_inv = 298.257223563

		x = x/1000 + self.x
  		y = y/1000 + self.y
  		z = z/1000 + self.z

  		p = sqrt(x*x + y*y);
  		r = sqrt(p*p + z*z);
  		f = 1/f_inv
  		e_2 = f*(2-f)
		u = atan((z/p)*((1-f)+e_2*a/r))

  		lat = atan((z*(1-f)+e_2*a*pow(sin(u),3))/((1-f)*(p-e_2*a*pow(cos(u),3))))
  		lat = lat * 180/pi

  		lon = atan(y/x)
  		lon = 180 + lon * 180/pi

  		alt = p*cos(lat) + z*sin(lat) - a*sqrt(1-e_2*sin(lat)*sin(lat))

		return lat,lon,alt